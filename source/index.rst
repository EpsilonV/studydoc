.. DataBook documentation master file, created by
   sphinx-quickstart on Mon Apr 30 10:57:27 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
    :glob:
    :maxdepth: 2

    front-end.rst
    algorithms/*
    database/index
    python/index
    go/index
    *
