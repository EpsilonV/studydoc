.. _IFE:

IFE学习笔记
=====================================================================

基础学院第一天
------------------------------
* 学习HTML
* 掌握CSS
* 掌握常见的布局方案
* 掌握JavaScript
* 学习一些MVVM框架，并能够自己独立造轮子


基础学院第二天
------------------------------



基础学院第七、八天  布局
------------------------------
两栏布局
~~~~~~~
#. 双 inline-block 布局
	使用css属性 :code:`display: inline-block` 将块状元素具有行内元素的属性,能够排列在一行。要注意行内元素的垂直对齐方式默认是baseline,所以要加上属性 :code:`vertical-align:top` .

.. code-block:: html

	<div class="container">
	   <div class="left"></div>
	   <div class="right"></div>
	</div>

	.container{
		height: 400px;
		font-size: 0;  //消除空格
	}
	.container .left,
	.container .right {
		height: 100%;
		display: inline-block;
		vertical-align: top;
		box-sizing: border-box;
	}
	.container .left {
		width: 200px;
		background-color: green;
	}
	.container .right {
		width: calc(100% - 200px);
		background-color: red;
	}

冲突的解决
  （如果git使用不熟练）建议在push不了时，pull之前。在本地创建一个新的分支并commit到local，以保证本地有commit记录，万一出什么问题，可以找回代码，以免代码丢失。

  （更甚者，把整个文件夹备份，不然出现找不回代码那就开心了）

	冲突的解决
  （如果git使用不熟练）建议在push不了时，pull之前。在本地创建一个新的分支并commit到local，以保证本地有commit记录，万一出什么问题，可以找回代码，以免代码丢失。

  （更甚者，把整个文件夹备份，不然出现找不回代码那就开心了）
